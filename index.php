<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">         
        <title>Productos</title>
        <!--CSS-->    
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css"/> 
       
         
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>          

    </head>
    <body>
        <div class="container">

            <h1 align="center">Productos</h1>     


            <a href="vista/Ventas.php" class="btn btn-primary pull-left menu">Listado de Ventas</a>
            <br><br><br><br>          

            <table class="table table-striped table-bordered">

                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Precio</th>
                    <th>Stock</th>
                    <th >Agregar a la  Venta</th >
                </tr>



                <?php
                require_once "model/Data.php";

                $d = new Data();

                $produtos = $d->getProductos();

                foreach ($produtos as $p) {
                    echo "<tr>";
                    echo "<td>" . $p->id . "</td>";
                    echo "<td>" . $p->nombre . "</td>";
                    echo "<td>$" . $p->precio . "</td>";
                    echo "<td>" . $p->stock . "</td>";
                    echo "<td>";
                    echo"<form action='controller/Agregar_ctrl.php' method='post'>";
                    echo"<input type='hidden' name='txtId' value='" . $p->id . "'>";
                    echo"<input type='hidden' name='txtNombre' value='" . $p->nombre . "'>";
                    echo"<input type='hidden' name='txtPrecio' value='" . $p->precio . "'>";
                    echo"<input type='hidden' name='txtStock' value='" . $p->stock . "'>";
                    echo"<input type='number' name='txtCantidad' required='required'> ";
                    echo"<input  type='submit' name='btnAgregar' value='Agregar' class='btn btn-warning'>";
                    echo "</form>";
                    echo"</td>";
                    echo "</tr>";
                }
                ?>    
            </table>


            <?php
            if (isset($_GET["m"])) {
                $m = $_GET["m"];
                switch ($m) {
                    case "1":
                        echo "El producto no tiene stock";
                        break;
                    case "2":
                        echo "La cantidad debe ser un numero positivo o mayor a cero";
                        break;
                }
            }
            ?>

            <?php
            session_start();
            if (isset($_SESSION["carrito"])) {
                $carrito = $_SESSION["carrito"];

                echo"<br><h1 align='center'>Compras</h1>";
                //echo"Cantidad de Objetos: ".count($carrito);

                echo "<table class='table table-striped table-bordered'>";

                echo"<tr>";

                echo"<th>ID</th>";
                echo"<th>Nombre</th>";
                echo"<th>Precio</th>";
                echo"<th>Stock</th>";
                echo"<th>Cantidad</th>";
                echo"<th>SubTotal</th>";
                echo"<th>Quitar</th>";

                echo"</tr>";

                $total = 0;
                $i = 0;

                foreach ($carrito as $p) {
                    echo "<tr>";
                    echo "<td>" . $p->id . "</td>";
                    echo "<td>" . $p->nombre . "</td>";
                    echo "<td>$" . $p->precio . "</td>";
                    echo "<td>" . $p->stock . "</td>";
                    echo "<td>" . $p->cantidad . "</td>";
                    echo "<td>$" . $p->subtotal . "</td>";
                    echo "<td>";
                    echo "<a href='controller/EliminarProdCarr_controll.php?in=$i' class='btn btn-danger'>Eliminar</a>";
                    echo"</td>";
                    $total += $p->subtotal;
                    $i++;
                    echo "</tr>";
                }

                echo "<tr>";
                echo "<td colspan='5' align='center'><b>TOTAL</b></td>";
                echo "<td colspan='2' align='center'><b>$$total</b></td>";
                $_SESSION["total"] = $total;
                echo "</tr>";

                echo "<tr>";
                echo "<td colspan='7' align='center'>";
                echo "<form action='controller/GenerarVenta_controll.php' method='post'>";
                echo "<input type='submit' value='Comprar' class='btn btn-success'>";
                echo "</form>";
                echo "</td>";
                echo "</tr>";
                echo "</table>";
            }
            ?>
        </div>
    </body>
</html>
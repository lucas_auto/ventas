<?php

//require_once "../model/Producto_model.php";
require_once "../model/Data.php";

$p = new Producto_model();

$p->cantidad = $_POST["txtCantidad"];

if ($p->cantidad > 0) {
    $p->id = $_POST["txtId"];
    $p->nombre = $_POST["txtNombre"];
    $p->precio = $_POST["txtPrecio"];
    $p->stock = $_POST["txtStock"];
    $p->subtotal = $p->precio * $p->cantidad;


    $d = new Data();


    session_start();

    if (isset($_SESSION["carrito"])) {
        $carrito = $_SESSION["carrito"];
    } else {
        $carrito = array();
    }

    $sumaCantidades = 0;
    foreach ($carrito as $pro) {
        if ($pro->id == $p->id) {
            $sumaCantidades += $pro->cantidad;
        }
    }
    $sumaCantidades += $p->cantidad;

    if ($p->stock >= $sumaCantidades) {
        //tengo stock
        array_push($carrito, $p);
        $_SESSION["carrito"] = $carrito;
        header("location: ../index.php");
    } else { //no tengo stock
        header("location: ../index.php?m=1");
    }
} else { //la cantidad es menor a 0
    header("location: ../index.php?m=2");
}
?>
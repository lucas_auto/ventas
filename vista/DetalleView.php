<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">     
        <title>Detalles</title>
        <!--CSS-->    
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css"/>       
    </head>
    <body>
        <div class="container">


<?php

        require_once "../model/Data.php";
        $idVenta = $_GET["id"];
        $d = new Data();

$detalles = $d->getDetalles($idVenta);

echo"<h1 align='center'>Detalles</h1>";

echo "<a href='../vista/Ventas.php' class='btn btn-primary'>Volver</a> <br> <br>";
     
echo "<table class='table table-striped table-bordered'>";
echo"<tr>";          
     echo"<th>ID</th>";
     echo"<th>Producto</th>";
     echo"<th>Cantidad</th>";
     echo"<th>SubTotal</th>";     
        
echo"</tr>";    
$total = 0;     
        foreach($detalles as $det){
echo "<tr>";
            echo "<td>".$det->id."</td>";
            echo "<td>".$det->nomProducto."</td>";
            echo "<td>".$det->cantidad." * $".$det->precio."</td>"; 
            echo "<td>$".$det->subtotal."</td>";  
            $total += $det->subtotal;                                             
echo "</tr>";
}  
                        echo "<tr>";                                                              
                        echo "<td colspan='3' align='center'><b>TOTAL</b></td>";
                        echo "<td  align='center'><b>$$total</b></td>";
                        $_SESSION["total"] = $total;
                        echo "</tr>";
echo "</table>";     




?>

                   </div>
    </body>
</html>
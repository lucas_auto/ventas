<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">     
        <title>Ventas</title>
        <!--CSS-->    
        <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css"/>   
        
    </head>
    <body>
        <div class="container">

         
            <?php
            
            require_once "../model/Data.php";
            $d = new Data();

            $ventas = $d->getVentas();

            echo"<h1 align='center'>Ventas</h1>";

            echo "<a href='../index.php' class='btn btn-primary'>Volver</a> <br> <br>";

            echo "<table class='table table-striped table-bordered'>";
            echo"<tr>";
            echo"<th>ID</th>";
            echo"<th>Fecha</th>";
            echo"<th>Total</th>";
            echo"<th>Detalle</th>";
            echo"</tr>";
            foreach ($ventas as $v) {
                echo "<tr>";
                echo "<td>" . $v->id . "</td>";
                echo "<td>" . $v->fecha . "</td>";
                echo "<td>$" . $v->total . "</td>";
                echo "<td>";
                echo "<a href='DetalleView.php?id=" . $v->id . "' class='btn btn-info'>Ver Detalle</a>";
                echo "</td>";
                echo "</tr>";
            }
            echo "</table>";
            ?>           
        </div>
    </body>
</html>
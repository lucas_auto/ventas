<?php

require_once "Conexion.php";
require_once "Producto_model.php";
require_once "Ventas_model.php";
require_once "Detalle.php";

class Data {

    private $con;

    public function __construct() {
        $this->con = new Conexion("localhost", "dbventas", "root", "");
    }

    public function getProductos() {
        $productos = array();

        $query = "select * from productos";
        $res = $this->con->ejecutar($query);

        while ($reg = mysql_fetch_array($res)) {
            $p = new Producto_model();
            //por fila 
            $p->id = $reg[0];
            $p->nombre = $reg[1];
            $p->precio = $reg[2];
            $p->stock = $reg[3];

            array_push($productos, $p); 
        }

        return $productos;
    }

    public function crearVenta($listaProductos, $total) {
        //crear la venta
        $query = "insert into ventas values(null,now(),$total)";
        $this->con->ejecutar($query);
        //rescatar ultima venta(id)
        $query = "select max(id) from ventas";
        $res = $this->con->ejecutar($query);
        $idUltimaVenta = 0;
        if ($reg = mysql_fetch_array($res)) {
            $idUltimaVenta = $reg[0];
        }
        //los insert en el detalle
        foreach ($listaProductos as $p) {
            $query = "insert into detalle_ventas values(null,                 
                  '" . $p->cantidad . "',
                  '" . $p->subtotal . "',
                  '" . $idUltimaVenta . "',
                  '" . $p->id . "')";

            $this->con->ejecutar($query);
            $this->actualizarStock($p->id, $p->cantidad);
        }
    }

    public function actualizarStock($id, $stockDescontar) {
        $query = "select stock from productos where id = $id";
        $res = $this->con->ejecutar($query);

        $stockActual = 0;

        if ($reg = mysql_fetch_array($res)) {
            $stockActual = $reg[0];
        }

        $stockActual -= $stockDescontar;

        $query = "update productos set stock = $stockActual where id = $id";
        $this->con->ejecutar($query);
    }

    public function getVentas() {
        $ventas = array();

        $query = "select * from ventas";
        $res = $this->con->ejecutar($query);

        while ($reg = mysql_fetch_array($res)) {
            $v = new Ventas_model();
            //por fila 
            $v->id = $reg[0];
            $v->fecha = $reg[1];
            $v->total = $reg[2];

            array_push($ventas, $v);
        }

        return $ventas;
    }

    public function getDetalles($idVenta) {

        $query = "select d.id, p.nombre, d.cantidad, d.subtotal, p.precio
                from detalle_ventas d, productos p 
                where d.idprod = p.id and d.idventa = $idVenta";

        $detalles = array();
        $res = $this->con->ejecutar($query);

        while ($reg = mysql_fetch_array($res)) {
            $d = new Detalle();
            //por fila 
            $d->id = $reg[0];
            $d->nomProducto = $reg[1];
            $d->cantidad = $reg[2];
            $d->subtotal = $reg[3];
            $d->precio = $reg[4];

            array_push($detalles, $d);
        }

        return $detalles;
    }

}

/*
  public function tieneStock($id,$stock){
  $query = "select stock from productos where id = $id";
  echo $query;
  echo "<br>";

  $res = $this->con->ejecutar($query);
  $stockActual = 0;

  if ($reg = mysql_fetch_array($res)){
  $stockActual = $reg[0];
  }

  echo "StockActual: ".$stockActual;
  echo "<br>";
  echo "Stock: ".$stock;
  echo "<br>";
  return $stockActual >= $stock;

  }

 */
?>